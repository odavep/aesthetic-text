# Aesthetic-Text

A stupid text formatter using Python/C#/Bash to turn text into AESTHETIC text

The project will essentially do this

text --> f(text) --> A E S T H E T I C   T E X T

The user may configure number of spaces, precursor and postcursor tags (such as the asterisks) through JSON or command line args.

